FROM chrismacp/ubuntu-10.04
ENV QTDIR /usr/lib/qt3
RUN echo "deb http://old-releases.ubuntu.com/ubuntu lucid main universe multiverse" > /etc/apt/sources.list; apt-get update
RUN apt-get -yf install wget unzip
RUN apt-get -yf install make g++
RUN apt-get -yf install libieee1284-3-dev libqt3-mt-dev libfftw3-dev
RUN wget http://www.mtoussaint.de/qtdso-0.3.1.tgz && tar zxvf qtdso-0.3.1.tgz && ls 
RUN apt-get install -yf libqt3-mt-dev
RUN cd QtDSO-0.3.1 && ./configure && make
CMD sleep infinity
